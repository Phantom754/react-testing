import { FC } from "react";

type GreetProp = {
  named:string;
}
const Greet: FC<GreetProp> = ({named}) => {
  return (
    <div>bonjour {named} </div>
  )
}

export default Greet;