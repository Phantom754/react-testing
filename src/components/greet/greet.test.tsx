import { render, screen } from "@testing-library/react"
import Greet from "./greet"

describe('greet group test', () => {
    it("greet render correctly",()=> {
      render(<Greet named="" />);
      const textElement = screen.getByText(/bonjour/i);
      expect(textElement).toBeInTheDocument();
    })
    describe('nested', () => {
       it('Greet render with should name', () => {
        render(<Greet named="Onjaniaina" />);
        const textElement = screen.getByText(/bonjour onja/i);
        expect(textElement).toBeInTheDocument();//nous attendons que cet element est dans le document
        })
    })

    it('Greet render with should name', () => {
      render(<Greet named="Onjaniaina" />);
      const textElement = screen.getByText(/bonjour onjaniaina/i);
      expect(textElement).toBeInTheDocument();//nous attendons que cet element est dans le document
      })
    
});

describe('nested', () => {
  test('Greet render with should name', () => {
   render(<Greet named="Onjaniaina" />);
   const textElement = screen.getByText(/jour onjaniaina/i);
   expect(textElement).toBeInTheDocument();//nous attendons que cet element est dans le document
   })
})
