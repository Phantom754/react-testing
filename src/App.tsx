import React from 'react';
import Greet from './components/greet/greet';

function App() {
  return (
    <div className="">
      <header className="App-header">
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
      <Greet />
    </div>
  );
}

export default App;
